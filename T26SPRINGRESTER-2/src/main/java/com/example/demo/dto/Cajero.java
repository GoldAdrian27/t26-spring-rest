package com.example.demo.dto;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="cajeros")
public class Cajero {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int codigo;
	@Column(name="nomApels")
	private String nomApels;
	
    @OneToMany(
            mappedBy = "cajero",
            cascade = CascadeType.PERSIST,
            fetch = FetchType.LAZY
        )
	 List<Venta> venta = new ArrayList<>();
	
	public Cajero() {
		
	}
	
	
	public Cajero(int codigo, String nomApels, List<Venta> venta) {
		super();
		this.codigo = codigo;
		this.nomApels = nomApels;
		this.venta = venta;
	}


	//Getters Setters
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNomApels() {
		return nomApels;
	}

	public void setNomApels(String nombre) {
		this.nomApels = nombre;
	}
	
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Venta")
	public List<Venta> getVenta() {
		return venta;
	}

	public void setVenta(List<Venta> suministra) {
		this.venta = suministra;
	}



	@Override
	public String toString() {
		return "Pieza [id=" + codigo + ", nomApels=" + nomApels + ", venta=" + venta + "]";
	}
	
	

	
	
}
