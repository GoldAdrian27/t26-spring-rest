package com.example.demo.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="venta")
public class Venta {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
    
	@ManyToOne
    @JoinColumn(name = "codigo")
    private Cajero cajero;

	@ManyToOne
    @JoinColumn(name = "codigo")
	private Productos productos;
	
	@ManyToOne
    @JoinColumn(name = "codigo")
	private MaquinaRegistradora maquinasRegistradoras;
	
	//Constructor
	public Venta() {
	
	}
	
	public Venta(int id, Cajero cajero, Productos productos, MaquinaRegistradora maquinasRegistradoras, int precio) {
		super();
		this.id = id;
		this.cajero = cajero;
		this.productos = productos;
		this.maquinasRegistradoras = maquinasRegistradoras;
	}
	//Getters y setters


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Cajero getCajero() {
		return cajero;
	}

	public void setCajero(Cajero cajero) {
		this.cajero = cajero;
	}

	public Productos getProductos() {
		return productos;
	}

	public void setProductos(Productos productos) {
		this.productos = productos;
	}

	public MaquinaRegistradora getMaquinasRegistradoras() {
		return maquinasRegistradoras;
	}

	public void setMaquinasRegistradoras(MaquinaRegistradora maquinasRegistradoras) {
		this.maquinasRegistradoras = maquinasRegistradoras;
	}



	@Override
	public String toString() {
		return "Venta [id=" + id + ", cajero=" + cajero + ", productos=" + productos + ", maquinasRegistradoras="
				+ maquinasRegistradoras + "]";
	}




	
	
	
}
