package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.MaquinaRegistradora;
import com.example.demo.service.MaquinaRegistradoraServiceImpl;


@RestController
@RequestMapping("/api")
public class MaquinaRegistradoraController {
	@Autowired
	MaquinaRegistradoraServiceImpl maquinaRegistradoraServiceImpl;

	@GetMapping("/maquinaRegistradoras")
	public List<MaquinaRegistradora> listarMaquinaRegistradoras(){
		return maquinaRegistradoraServiceImpl.listarMaquinaRegistradora();
	}
	
	@GetMapping("/maquinaRegistradoras/{id}")
	public MaquinaRegistradora maquinaRegistradoraXID(@PathVariable(name="id") int id){
		return maquinaRegistradoraServiceImpl.maquinaRegistradoraXID(id);
	}
	
	@PostMapping("/maquinaRegistradoras")
	public MaquinaRegistradora guardarMaquinaRegistradora(@RequestBody MaquinaRegistradora maquinaRegistradora) {
		
		return maquinaRegistradoraServiceImpl.guardarMaquinaRegistradora(maquinaRegistradora);
	}
	
	@PutMapping("/maquinaRegistradoras/{id}")
	public MaquinaRegistradora actualizarMaquinaRegistradora(@PathVariable(name="id") int id, @RequestBody MaquinaRegistradora maquinaRegistradora) {
		MaquinaRegistradora maquinaRegistradora_seleccionada = new MaquinaRegistradora();
		MaquinaRegistradora maquinaRegistradora_actualizado = new MaquinaRegistradora();
		
		maquinaRegistradora_seleccionada = maquinaRegistradoraServiceImpl.maquinaRegistradoraXID(id);
		
		maquinaRegistradora_seleccionada.setPiso(maquinaRegistradora.getPiso());
		
		maquinaRegistradora_actualizado = maquinaRegistradoraServiceImpl.actualizarMaquinaRegistradora(maquinaRegistradora_seleccionada);
		
		return maquinaRegistradora_actualizado;
	}
	
	@DeleteMapping("/maquinaRegistradoras/{id}")
	public void eliminarMaquinaRegistradora(@PathVariable(name="id")int id) {
		maquinaRegistradoraServiceImpl.eliminarMaquinaRegistradora(id);
	}
}
