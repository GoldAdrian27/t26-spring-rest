package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Venta;
import com.example.demo.service.VentaServiceImpl;

@RestController
@RequestMapping("/api")
public class VentaController {
	@Autowired
	VentaServiceImpl ventaServiceImpl;

	@GetMapping("/venta")
	public List<Venta> listarProveedor(){
		return ventaServiceImpl.listarVenta();
	}
	
	@GetMapping("/venta/{id}")
	public Venta proveedorXID(@PathVariable(name="id") int id){
		return ventaServiceImpl.ventaXID(id);
	}
	
	@PostMapping("/venta")
	public Venta guardarProveedor(@RequestBody Venta venta) {
		
		return ventaServiceImpl.guardarVenta(venta);
	}
	
	@PutMapping("/venta/{id}")
	public Venta actualizarProveedores(@PathVariable(name="id") int id, @RequestBody Venta venta) {
		Venta venta_seleccionada = new Venta();
		Venta venta_actualizado = new Venta();
		
		venta_seleccionada = ventaServiceImpl.ventaXID(id);
		
		venta_seleccionada.setCajero(venta.getCajero());
		venta_seleccionada.setProductos(venta.getProductos());
		
		venta_actualizado = ventaServiceImpl.actualizarVenta(venta_seleccionada);
		
		return venta_actualizado;
	}
	
	@DeleteMapping("/venta/{id}")
	public void eliminarProveedor(@PathVariable(name="id")int id) {
		ventaServiceImpl.eliminarVenta(id);
	}

}
