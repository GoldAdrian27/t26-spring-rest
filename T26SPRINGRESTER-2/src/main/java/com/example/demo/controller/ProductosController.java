package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Productos;
import com.example.demo.service.ProductosServiceImpl;


@RestController
@RequestMapping("/api")
public class ProductosController {
		@Autowired
		ProductosServiceImpl proveedorServiceImpl;

		@GetMapping("/productos")
		public List<Productos> listarProveedor(){
			return proveedorServiceImpl.listarProductos();
		}
		
		@GetMapping("/productos/{id}")
		public Productos proveedorXID(@PathVariable(name="id") int id){
			return proveedorServiceImpl.productosXID(id);
		}
		
		@PostMapping("/productos")
		public Productos guardarProveedor(@RequestBody Productos proveedor) {
			
			return proveedorServiceImpl.guardarProductos(proveedor);
		}
		
		@PutMapping("/productos/{id}")
		public Productos actualizarProductos(@PathVariable(name="id") int id, @RequestBody Productos proveedor) {
			Productos proveedor_seleccionada = new Productos();
			Productos proveedor_actualizado = new Productos();
			
			proveedor_seleccionada = proveedorServiceImpl.productosXID(id);
			
			proveedor_seleccionada.setNombre(proveedor.getNombre());
			proveedor_seleccionada.setPrecio(proveedor.getPrecio());
			
			proveedor_actualizado = proveedorServiceImpl.actualizarProductos(proveedor_seleccionada);
			
			return proveedor_actualizado;
		}
		
		@DeleteMapping("/productos/{id}")
		public void eliminarProveedor(@PathVariable(name="id")int id) {
			proveedorServiceImpl.eliminarProductos(id);
		}
	
}
