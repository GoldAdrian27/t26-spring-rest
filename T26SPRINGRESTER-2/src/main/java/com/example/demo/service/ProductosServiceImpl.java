package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IProductosDAO;
import com.example.demo.dto.Productos;

@Service
public class ProductosServiceImpl implements IProductosServiceImpl{
	@Autowired
	IProductosDAO iproductosDao;
	
	@Override
	public List<Productos> listarProductos() {
		return iproductosDao.findAll();
		}

	@Override
	public Productos guardarProductos(Productos proveedor) {
		return iproductosDao.save(proveedor);
	}

	@Override
	public Productos productosXID(int codigo) {
		return iproductosDao.findById(codigo).get();
	}

	@Override
	public Productos actualizarProductos(Productos proveedor) {
		return iproductosDao.save(proveedor);
	}

	@Override
	public void eliminarProductos(int codigo) {
		iproductosDao.deleteById(codigo);	
	}

}
