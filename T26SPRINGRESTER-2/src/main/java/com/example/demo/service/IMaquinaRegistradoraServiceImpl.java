package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.MaquinaRegistradora;


public interface IMaquinaRegistradoraServiceImpl {
	public List<MaquinaRegistradora> listarMaquinaRegistradora(); 
	
	public MaquinaRegistradora  guardarMaquinaRegistradora(MaquinaRegistradora proveedor);	
	
	public MaquinaRegistradora maquinaRegistradoraXID(int id); 
	
	public MaquinaRegistradora actualizarMaquinaRegistradora(MaquinaRegistradora proveedor); 
	
	public void eliminarMaquinaRegistradora(int id);
}
