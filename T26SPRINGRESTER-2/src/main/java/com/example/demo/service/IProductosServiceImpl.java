package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Productos;


public interface IProductosServiceImpl {
	public List<Productos> listarProductos(); 
	
	public Productos  guardarProductos(Productos proveedor);	
	
	public Productos productosXID(int id); 
	
	public Productos actualizarProductos(Productos proveedor); 
	
	public void eliminarProductos(int id);
}
