package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.ICajeroDAO;
import com.example.demo.dto.Cajero;

@Service
public class CajeroServiceImpl implements ICajeroServiceImpl{
	
	@Autowired
	ICajeroDAO iCajeroDao;

	@Override
	public List<Cajero> listarCajero() {
		return iCajeroDao.findAll();
	}

	@Override
	public Cajero guardarCajero(Cajero cajero) {
		return iCajeroDao.save(cajero);
	}

	@Override
	public Cajero cajeroXID(int codigo) {
		return iCajeroDao.findById(codigo).get();
	}

	@Override
	public Cajero actualizarCajero(Cajero cajero) {
		return iCajeroDao.save(cajero);
	}

	@Override
	public void eliminarCajero(int codigo) {
		iCajeroDao.deleteById(codigo);
	}
}
