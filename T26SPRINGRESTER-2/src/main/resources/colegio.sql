DROP TABLE IF EXISTS `venta`;
DROP TABLE IF EXISTS `cajeros`;
DROP TABLE IF EXISTS `productos`;
DROP TABLE IF EXISTS `maquinas_registradoras`;

CREATE TABLE `cajeros` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `nomApels` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
);

INSERT INTO `cajeros` VALUES (1,'cajero 1'),(2,'cajero 2'),(3,'cajero 3'),(4,'cajero 4'),(5,'cajero 5'),(6,'cajero 6');


CREATE TABLE `productos` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `precio` int NOT NULL,
  PRIMARY KEY (`codigo`)
);

INSERT INTO `productos` VALUES (1,'producto 1', 545),(2,'producto 2', 545),(3,'producto 3', 545),(4,'producto 4', 545),(5,'producto 5', 545);

CREATE TABLE `maquinas_registradoras` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `piso` int DEFAULT NULL,
  PRIMARY KEY (`codigo`)
);

INSERT INTO `maquinas_registradoras` VALUES (1,3),(2, 2),(3,45),(4,3),(5,5),(6,6);


CREATE TABLE `venta` (
  `id` int NOT NULL AUTO_INCREMENT,
  `codigo_cajero` int NOT NULL,
  `codigo_productos` int NOT NULL,
  `codigo_maquinas_registradoras` int NOT NULL,
  CONSTRAINT `codigo_cajero_FK` FOREIGN KEY (`codigo_cajero`) REFERENCES `cajeros` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `codigo_productos_FK` FOREIGN KEY (`codigo_productos`) REFERENCES `productos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `codigo_maquinas_registradoras_FK` FOREIGN KEY (`codigo_maquinas_registradoras`) REFERENCES `maquinas_registradoras` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (`id`)

);

INSERT INTO `venta` VALUES (1,2,1,1),(2,1,2,2),(3,3,3,3),(4,2,3,4),(5,4,3,5);
