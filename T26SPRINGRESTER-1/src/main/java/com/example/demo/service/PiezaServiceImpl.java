package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IPiezaDAO;
import com.example.demo.dto.Pieza;

@Service
public class PiezaServiceImpl implements IPiezaServiceImpl{
	@Autowired
	IPiezaDAO iPiezaDao;

	@Override
	public List<Pieza> listarPieza() {
		return iPiezaDao.findAll();
	}

	@Override
	public Pieza guardarPieza(Pieza pieza) {
		return iPiezaDao.save(pieza);
	}

	@Override
	public Pieza piezaXID(int codigo) {
		return iPiezaDao.findById(codigo).get();
	}

	@Override
	public Pieza actualizarPieza(Pieza pieza) {
		return iPiezaDao.save(pieza);
	}

	@Override
	public void eliminarPieza(int codigo) {
		iPiezaDao.deleteById(codigo);
	}
}
