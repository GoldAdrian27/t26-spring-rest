package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Proveedor;


public interface IProveedoresServiceImpl {
	public List<Proveedor> listarProveedores(); 
	
	public Proveedor  guardarProveedores(Proveedor proveedor);	
	
	public Proveedor proveedoresXID(int id); 
	
	public Proveedor actualizarProveedores(Proveedor proveedor); 
	
	public void eliminarProveedores(int id);
}
