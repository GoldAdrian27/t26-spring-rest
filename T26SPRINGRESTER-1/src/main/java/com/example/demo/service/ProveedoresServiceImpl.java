package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IProveedoresDAO;
import com.example.demo.dto.Proveedor;

@Service
public class ProveedoresServiceImpl implements IProveedoresServiceImpl{
	@Autowired
	IProveedoresDAO iproveedoresDao;
	
	@Override
	public List<Proveedor> listarProveedores() {
		return iproveedoresDao.findAll();
		}

	@Override
	public Proveedor guardarProveedores(Proveedor proveedor) {
		return iproveedoresDao.save(proveedor);
	}

	@Override
	public Proveedor proveedoresXID(int id) {
		return iproveedoresDao.findById(id).get();
	}

	@Override
	public Proveedor actualizarProveedores(Proveedor proveedor) {
		return iproveedoresDao.save(proveedor);
	}

	@Override
	public void eliminarProveedores(int id) {
		iproveedoresDao.deleteById(id);	
	}

}
