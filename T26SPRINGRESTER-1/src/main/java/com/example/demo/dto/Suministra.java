package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="suministra")
public class Suministra {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@OneToMany
    @JoinColumn(name = "codigo")
    Pieza pieza;
	
	@OneToMany
    @JoinColumn(name = "id")
    Proveedor proveedor;
    
	@Column(name = "precio")
	private int precio;
	
	//Constructor
	public Suministra() {
	
	}

	public Suministra(int id, Pieza codigo_pieza, Proveedor idProveedor, int precio) {
		super();
		this.id = id;
		this.pieza = codigo_pieza;
		this.proveedor = idProveedor;
		this.precio = precio;
	}
	
	//Getters y setters
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Pieza getCodigo_pieza() {
		return pieza;
	}
	
	public void setCodigo_pieza(Pieza codigo_pieza) {
		this.pieza = codigo_pieza;
	}
	
	public Proveedor getProveedorId() {
		return proveedor;
	}
	
	public void setProveedorId(Proveedor proveedorId) {
		this.proveedor = proveedorId;
	}
	
	public int getPrecio() {
		return precio;
	}
	
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	
	
	
}
