package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Suministra;
import com.example.demo.service.SuministraServiceImpl;

@RestController
@RequestMapping("/api")
public class SuministraController {
	@Autowired
	SuministraServiceImpl suministraServiceImpl;

	@GetMapping("/suministra")
	public List<Suministra> listarProveedor(){
		return suministraServiceImpl.listarSuministra();
	}
	
	@GetMapping("/suministra/{id}")
	public Suministra proveedorXID(@PathVariable(name="id") int id){
		return suministraServiceImpl.suministraXID(id);
	}
	
	@PostMapping("/suministra")
	public Suministra guardarProveedor(@RequestBody Suministra suministra) {
		
		return suministraServiceImpl.guardarSuministra(suministra);
	}
	
	@PutMapping("/suministra/{id}")
	public Suministra actualizarProveedores(@PathVariable(name="id") int id, @RequestBody Suministra suministra) {
		Suministra suministra_seleccionada = new Suministra();
		Suministra suministra_actualizado = new Suministra();
		
		suministra_seleccionada = suministraServiceImpl.suministraXID(id);
		
		suministra_seleccionada.setProveedorId(suministra.getProveedorId());
		suministra_seleccionada.setCodigo_pieza(suministra.getCodigo_pieza());
		suministra_seleccionada.setPrecio(suministra.getPrecio());
		
		suministra_actualizado = suministraServiceImpl.actualizarSuministra(suministra_seleccionada);
		
		return suministra_actualizado;
	}
	
	@DeleteMapping("/suministra/{id}")
	public void eliminarProveedor(@PathVariable(name="id")int id) {
		suministraServiceImpl.eliminarSuministra(id);
	}

}
