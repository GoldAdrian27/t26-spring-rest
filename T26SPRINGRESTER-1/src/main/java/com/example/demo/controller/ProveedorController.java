package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Proveedor;
import com.example.demo.service.ProveedoresServiceImpl;


@RestController
@RequestMapping("/api")
public class ProveedorController {
		@Autowired
		ProveedoresServiceImpl proveedorServiceImpl;

		@GetMapping("/proveedores")
		public List<Proveedor> listarProveedor(){
			return proveedorServiceImpl.listarProveedores();
		}
		
		@GetMapping("/proveedores/{id}")
		public Proveedor proveedorXID(@PathVariable(name="id") int id){
			return proveedorServiceImpl.proveedoresXID(id);
		}
		
		@PostMapping("/proveedores")
		public Proveedor guardarProveedor(@RequestBody Proveedor proveedor) {
			
			return proveedorServiceImpl.guardarProveedores(proveedor);
		}
		
		@PutMapping("/proveedores/{id}")
		public Proveedor actualizarProveedores(@PathVariable(name="id") int id, @RequestBody Proveedor proveedor) {
			Proveedor proveedor_seleccionada = new Proveedor();
			Proveedor proveedor_actualizado = new Proveedor();
			
			proveedor_seleccionada = proveedorServiceImpl.proveedoresXID(id);
			
			proveedor_seleccionada.setNombre(proveedor.getNombre());
			
			proveedor_actualizado = proveedorServiceImpl.actualizarProveedores(proveedor_seleccionada);
			
			return proveedor_actualizado;
		}
		
		@DeleteMapping("/proveedores/{id}")
		public void eliminarProveedor(@PathVariable(name="id")int id) {
			proveedorServiceImpl.eliminarProveedores(id);
		}
	
}
