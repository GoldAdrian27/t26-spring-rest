DROP TABLE IF EXISTS `suministra`;
DROP TABLE IF EXISTS `piezas`;
DROP TABLE IF EXISTS `proveedores`;

CREATE TABLE `piezas` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
);

INSERT INTO `piezas` VALUES (1,'pieza 1'),(2,'pieza 2'),(3,'pieza 3'),(4,'pieza 4'),(5,'pieza 5'),(6,'pieza 6');


CREATE TABLE `proveedores` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
);


INSERT INTO `proveedores` VALUES (1,'proveedor 1'),(2,'proveedor 2'),(3,'proveedor 3'),(4,'proveedor 4'),(5,'proveedor 5');

CREATE TABLE `suministra` (
  `id` int NOT NULL AUTO_INCREMENT,
  `codigo_pieza` int NOT NULL,
  `id_proveedor` int NOT NULL,
  `precio` int NOT NULL,
  CONSTRAINT `codigo_pieza_FK` FOREIGN KEY (`codigo_pieza`) REFERENCES `piezas` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `id_proveedor_FK` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (`id`)

);

INSERT INTO `suministra` VALUES (1,2,1, 121),(2,1,2, 212),(3,3,3, 333),(4,2,3, 423),(5,4,3, 543);
